extends TileMap

var SPEED = 10
var TICK = 0
var ROW = 11
var WIDTH = 5
var COL = 8
var DIRECTION = "left"
var SCORE = 0
var GAME_OVER = false

func _ready():
	set_process(true)
	set_process_input(true)

func _process(delta):
	TICK += 1
	if TICK % SPEED == 0:
		_move()

func _input(event):
	var Event = make_input_local(event)
	if (event.is_action_pressed("ui_accept") or
			(event.type == InputEvent.SCREEN_TOUCH and Event.pressed)):
		if not GAME_OVER:
			_place()
		else:
			_reset()

func _move():
	if DIRECTION == "left":
		for i in range(12):
			var tile = -1
			if i >= COL and i < COL + WIDTH:
				tile = 0
			set_cell(i, ROW, tile)
		COL -= 1
		if COL < 0:
			DIRECTION = "right"
	else:
		COL += 1
		set_cell(COL + WIDTH, ROW, 0)
		set_cell(COL, ROW, -1)
		if COL + WIDTH == 9:
			DIRECTION = "left"

func _new_row():
	ROW -= 1
	DIRECTION = "left"
	COL = 9
	SCORE += 1

	# Move camera if we reach middle
	if ROW < 6:
		var cam = get_node("Camera2D")
		var pos = cam.get_offset()
		pos.y -= 64
		cam.set_offset(pos)
	if SPEED > 3 and ROW % 7 == 0:
		SPEED -= 1

	# Set score
	get_node("../CanvasLayer/Label").set_text(str(SCORE))

func _place():
	if ROW == 11:
		_new_row()
		return

	for i in range(COL + 1, COL + WIDTH + 1):
		if get_cell(i, ROW + 1) == -1:
			set_cell(i, ROW, -1)
			WIDTH -= 1

	if WIDTH == 0:
		GAME_OVER = true
		get_node("../CanvasLayer/Label").set_text(str(SCORE) + "\nGAME OVER")
	else:
		_new_row()


func _reset():
	for i in range(ROW, 12):
		for j in range(10):
			set_cell(j, i, -1)

	SPEED = 10
	TICK = 0
	ROW = 11
	WIDTH = 5
	COL = 8
	DIRECTION = "left"
	SCORE = 0
	GAME_OVER = false
	get_node("Camera2D").set_offset(Vector2(0,0))
	get_node("../CanvasLayer/Label").set_text(str(SCORE))
